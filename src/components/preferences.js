var fs = require('fs');
var path = require('path');
var preferencesFile = '../../preferences.json';

// Loads preferences and expose as exports
exports.load = function () {
    var data = require(preferencesFile);
    exports.revision = data.revision; 
    exports.stops = data.stops;  
}


// Saves preferences
exports.save = function () {
    exports.revision++;
    var data = {
        revision: exports.revision,
        stops: exports.stops
    };
    fs.writeFile(__dirname + '/' + preferencesFile, JSON.stringify(data, " ", 4));
}


// add preferred bus stop
exports.addStop = function(value) {
    var number = (value || "").toString().trim();
    if (number != "") {
        if (exports.stops.indexOf(number) == -1) {
            exports.stops.push(number);
            exports.save();
            return true;
        }
    }
    return false;
}


// remove bus stop from preferences
exports.removeStop = function(value) {
    var number = (value || "").toString().trim();
    if (number != "") {
        var i = exports.stops.indexOf(number); 
        if (i > -1) {
            exports.stops.splice(i, 1);
            exports.save();
            return true;
        }
    }
    return false;
}


