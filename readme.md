#Dublin Bus CLI

##Introduction
Command-line client for Dublin Bus. Pure NodeJS goodness for retrieving real-time information 
about buses across Dublin. Absolutely essential, if you use public transportation in this 
chronically congested city. And cool as well. After all, how many people use a terminal app 
to check the whereabouts of that feckin' bus? It even works on your Android phone, as shown below!

If you love it, why not rewarding me by a €2 PayPal donation? 
Every two and a half donation buys me a pint of [the good black stuff](https://en.wikipedia.org/wiki/Guinness) :-) 

* [PayPal.me €2 at https://www.paypal.me](https://www.paypal.me/tomaszwaraksa/2)
* [PayPal transfer at https://www.paypal.com using my e-mail address *tomasz@waraksa.net*](https://www.paypal.com/myaccount/transfer/send) 

Your name will be added to the list of donors and you'll be famous forever, as Internet never forgets! 

Merci beaucoup!

Tomasz 

##Output
This is how it looks like. Dublin Bus loves lolcat :-) 

![Dublin Bus loves lolcat](https://bytebucket.org/agilisconsultinglimited/dublin-bus/raw/ded8cceada9a70a11ad21662b5df96552324b0a9/images/output.png)

##Requirements
To run the application, NodeJS must be available on your system. You can find it here: https://nodejs.org/en or install using the package manager of your choice. 

On Debian-based Linux machines:

    sudo apt install nodejs nodejs-legacy

On Mac OS X:

    brew install node

Detailed instructions here: https://nodejs.org/en/download/package-manager/

##Installation
Install the application using npm, to make it available as global command   

    sudo npm install dublin-bus -g

If you're rather interested in source code, install it as local npm module in your preferred folder
for funky projects and hacks:

    npm install dublin-bus 
    cd ./node_modules/dublin-bus
    npm install

Alternatively you can pull the project directly from git repository: 

    git clone https://bitbucket.org/agilisconsultinglimited/dublin-bus.git
    cd dublin-bus
    npm install
    
##Usage    

Usage: dublin-bus [options] [command]

Options:

    -V, --version  output the application version number
    -d, --debug    Display debug information

Commands:

    stop <number>                   Displays real-time departures for the specified stop
    find <text>                     Finds stop by name and displays real-time departures
                                    If more matches found, you will be asked to choose.
    route <number> [operator]       Displays route details and all its stops. Dublin Bus
                                    operator is assumed, unless specified otherwise.
    timetable <stop> <route> [day]  Displays timetable for the specified stop, route and 
                                    day. If day is not specified, today is assumed.
    help                            Displays help
    preferences [...]               Commands for handling preferences, below more details

Examples:

    dublin-bus stop 1305            Displays departures at stop 1305
    dublin-bus find "Grange"        Finds stops with "Grange" in name
    dublin-bus route 17             Displays details and stop list of bus route 17
    dublin-bus timetable 1305 17    Displays todas timetable for stop 1305 and bus 17
    dublin-bus timetable 1305 17 7  Displays Sunday timetable for stop 1305 and bus 17
    dublin-bus preferences list     Displays application preferences

Shorthand syntax is available for quick lookups. Just specify bus number or name without any 
commands or options:

    dublin-bus 1305
    dublin-bus "Grange Road"    


##Cowsay mode
The app works nicely with cowsay, if you only ask cowsay to keep formatting
of the text it receives from dublin-bus application, by adding -n switch.
Cowsay also removes all the colors, but there's always lolcat for that ;-)

Examples

    dublin-bus stop 1305 | cowsay -n
    dublin-bus route 104 | cowsay -n | lolcat

Isn't it cute?

![Cow says Dublin Bus](https://bytebucket.org/tomaszwaraksa/dublin-bus-cli/raw/48a4524a3ded23807e34dd57d4a0291ebdbe3b38/docs/cowsay.png)

##Make it work on Android phone
Yes, it's possible! Follow these steps:

* Install Termux application on Android Phone, see [https://termux.com](https://termux.com). Termux 
  emulates CTRL, arrow keys etc. using various combinations of volume keys, check [https://termux.com/help.html](https://termux.com/help.html) 
  for details 
* You might want to install Hacker's Keyboard, which gives easier access to special keys, see 
  [https://play.google.com/store/apps/details?id=org.pocketworkstation.pckeyboard](https://play.google.com/store/apps/details?id=org.pocketworkstation.pckeyboard) 

Launch Termux and execute the following commands to prepare the environment and NodeJS platform:

    apt update && apt upgrade
    apt install coreutils
    apt install nodejs
    apt install cowsay
    node --version

The last command should display version number of the installed NodeJS platform. 
If it does, proceed with installing the application using the same procedure as on a desktop system.
If you choose to use git method, first add git to your phone by running:

    apt install git  

Go to the folder where application is installed. If you used npm method, it's:

    cd node_modules/dublin-bus-cli

and for git method it's

    cd dublin-bus

Launch the application using node intepreter, for example to get the bus stop 1305 information:

    node dublin-bus.js 1305
    node dublin-bus.js stop 1305 | cowsay -n

And this is the result:

![Cow says Dublin Bus on Android Phone](https://bytebucket.org/tomaszwaraksa/dublin-bus-cli/raw/5ec3c5b38e6493cd822bf344c8d9ac0c9c1b8506/docs/dublinbusandroid.jpeg)

##Preferences
In *preferences.json* file you can store various default settings for the application:
###stops
A list of your favourite bus stops. If specified, it will be loaded automatically, if you launch the 
application without any input parameters. Easy way to quickly check current departures at your usual places.

###Displaying preferences
To display current preferences, use the following command:

    dublin-bus preferences list     

To add or remove preferred bus stops, use the following commands:

    dublin-bus preferences stop add <number>
    dublin-bus preferences stop remove <number>         

Note that the above will modify preferences.json file. If you have installed the application as global command, you might receive access denied errors. Run the commands with sudo in such case:

    sudo dublin-bus preferences stop add <number>
    sudo dublin-bus preferences stop remove <number>