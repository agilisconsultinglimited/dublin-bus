var q = require('q');
var chalk = require('chalk');

// DATA LOGGER
exports.options = {};

// Logged messages
exports.history = [];

// Logs a message
exports.log = function (message, data) {
    if (message) {
        if (exports.options.debug)
            console.log(chalk.gray(message), data ? data : "");
        exports.history.push({
            time: new Date(),
            message: message,
            data: data
        });
    }
    else {
        if (exports.options.debug)
            console.log();        
    }
    return q.when(data);
};
        
// Logs an information
exports.info = function (message, data) {
    return exports.log(chalk.green(message), data);
};
        
// Logs an error
exports.error = function (message, data) {
    return exports.log(chalk.red(message), data);
};
        
