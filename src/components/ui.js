var q = require('q');
var prompt = require('prompt');
var chalk = require('chalk');
var format = require('stringformat');
format.extendString();
var logger = require('./logger');
var dataService = require('./data-service');
var preferences = require('./preferences');   

// DATA RENDERER FOR CONSOLE
exports.options = {};

// Renders empty line
exports.line = function (data) {
    console.log(' ');
    return q.when(data);
}

// Renders text line
function text(text, data) {
    if (!text || text == '')
        text = ' ';
    console.log(text);
    return q.when(data);
}
exports.text = text;

// Displays stop information
exports.displayStop = function (data) {
    var stopId = null;
    displayResults(data, function() {
        data.results.forEach(function(stop) {
            stopId = stop.stopid;
            roundGeoPosition(stop, 4);
            text(chalk.gray('Stop #   ') + chalk.green('{stopid}').format(stop));
            text(chalk.gray('Name     ') + chalk.green('{shortname}').format(stop));
            text(chalk.gray('Position ') + chalk.green('{latitude}, {longitude}').format(stop));
            stop.operators.forEach(function(operator) {
                text(chalk.gray('Routes   ') + chalk.green('{0}').format(operator.routes.join(", ")));
            });
        });
    });
    return q.when(stopId); 
}

// Displays stop departures information
exports.displayDepartures = function (data) {
    var stopId = null;    
    displayResults(data, function() {
        stopId = data["stopid"];
        data.results.forEach(function(result) {
            result.duetext = result.duetime == 'Due' ? 'Due' : (result.duetime + 'm');
            text((chalk.cyan('{duetext:5}') + chalk.green('{route:4} to {destination}')).format(result));
        }, this);
    });
    return q.when(stopId); 
}

// Displays route
exports.displayRoute = function (data) {
    var routeId = null;
    displayResults(data, function() {
        data.results.forEach(function(route) {
            routeId = data["route"];
            text(chalk.gray('Route       ') + chalk.green('{route}').format(data));
            text(chalk.gray('Origin      ') + chalk.green('{origin}').format(route));
            text(chalk.gray('Destination ') + chalk.green('{destination}').format(route));
            text();
            route.stops.forEach(function(stop) {
                text((chalk.cyan('{stopid:4}.') + chalk.green(' {shortname}')).format(stop));
            });
            text();
        });
    });
    return q.when(routeId); 
}

// Displays day timetable
exports.displayDayTimetable = function(timetable) {       
    // display        
    text(chalk.gray('Day     ') + chalk.green('{name}').format(timetable)); 
    text(chalk.gray('Stop    ') + chalk.green('{stopId}').format(timetable)); 
    text(chalk.gray('Route   ') + chalk.green('{routeId}').format(timetable));
    text(chalk.gray('Towards ') + chalk.green('{defaultDestinationName}').format(timetable));
    text();
    text(chalk.red('WARNING! Timetable API is buggy and often returns redundant data!'));
    text();
    var matrix = "";
    var maxColumns = 8;
    var column = 1;
    var destinationIndex = 0;
    var destinationCode = 'abcdefghijklmnopqrstuvwxyz';        

    timetable.departures.forEach(function(departure) {
        var destination = timetable.destinations[departure.destination];
        destination.used = true;
        if (!destination.code)
            destination.code = destination.isDefault ? " " : destinationCode[destinationIndex++]; 
        matrix = matrix + chalk.green(departure.time.substring(0, 5)) + chalk.white(destination.code) + "  ";
        column++;
        if (column>maxColumns) {
            matrix = matrix + "\n";
            column = 1;
        }
    });
    text(matrix);
    text();
    for (var name in timetable.destinations) {
        var destination = timetable.destinations[name];
        if (!destination.isDefault && destination.used)
            text((chalk.white('{code}') + chalk.green(' {name}')).format(destination));
    }
}

// Selects stop from the specified list.
// If list only contains one stop, it will be returned immediately. 
exports.selectStop = function(stops) {
    return q.Promise(function(resolve, reject) {
        if (!stops || !stops.length) {
            text(chalk.red(messages.noStopsFound));
            reject(stops); 
        }
        else if (stops.length == 1) {
            resolve(stops[0].code);
        }
        else {
            text();
            text(chalk.yellow('{0} stops found:'.format(stops.length)));
            text();
            stops.forEach(function(item) {
                text(chalk.green('{code:4}'.format(item)) + ': {name}'.format(item));
            }, this);
            text();
            prompt.message = '';
            prompt.colors = false;
            prompt.start();
            prompt.get([{
                name: 'stopid',
                description: 'Stop to query',
                type: 'string'
            }], function(error, result) {                
                if (error) {
                    reject(stops);
                }
                else {
                    text();
                    var stop = stops.filter(function(s) {
                        return s.code == result.stopid; 
                    });                    
                    if (stop.length == 0) {
                        reject(stops);
                    }
                    else {
                        resolve(stop[0].code);
                    }
                }
            });
        }
    });
}

// Displays information that no data were found matching user criteria
exports.noDataFound = function() {
    return exports.text(chalk.red('No data found'));
}

// Displays information that no stops were found matching user criteria
exports.noStopsFound = function() {
    return exports.text(chalk.red('No matching stops found'));
}

// Displays preferences 
exports.displayPreferences = function() {
    exports.text(chalk.gray("Preferences"));
    exports.text(chalk.gray("Revision: ") + chalk.green(preferences.revision));
    exports.text(chalk.gray("Stops:    ") + chalk.green(preferences.stops.join(", ")));
}

// Displays information that no stops were found matching user criteria
exports.preferencesUpdated = function() {
    return exports.text(chalk.yellow('Preferences updated.'));
}

// Rounds lat/lon information to a more human-readable precision
function roundGeoPosition(data, precision) {
    if (data && data.latitude && data.longitude) {
        if (!precision)
            precision = 2;
        var multiplier = Math.pow(10.0, precision);
        data.latitude = Math.round(data.latitude * multiplier) / multiplier;
        data.longitude = Math.round(data.longitude * multiplier) / multiplier;        
    }
} 

// Displays error returned by the request
function displayError(error) {
    if (error && error.errorCode) {
        text(chalk.red(messages.error));
        text(chalk.red("Code: {errorCode}".format(error)));
        if (error.errorMessage)
            text(chalk.red("Message: {errorMessage}".format(error)));
    }
}

// Displays results returned by the request
function displayResults(data, onSuccess) {
    if (dataService.isError(data))  
        displayError(data);
    else if (!dataService.hasResults(data)) 
        text(messages.noResults.yellow);
    else
        if (onSuccess)
            onSuccess(data);
}




    